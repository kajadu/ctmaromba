$(function () {
    $(document).on("click",".consultar-treino-aluno",function () {
        var id_aluno = $(this).attr("id_aluno");
        window.location.href = "/treino_aluno_consulta/"+id_aluno;
    });
    $(document).on("click",".atribuir-treino",function () { 
        var id_aluno = $(this).attr("id_aluno");        
        $("#id_aluno_modal").val(id_aluno);
    });
    
    
    $(".tipo-exercicio").autocomplete({     
        source: function( request, response ) {
            $.ajax( {
              url: "/servicos_background/autocomplete/exercicios.php",
              dataType: "json",
              data: {
                term: request.term
              },
              success: function( data ) {
                response( data );
              }
            } );
        },        
        minLength: 2,
        select: function( event, ui ) {            
            $("#musculo_exercicio_"+$(this).attr("treino")+"_"+$(this).attr("linha_treino")).html(ui.item.musculo);
            $("#id_tipo_exercicio_"+$(this).attr("treino")+"_"+$(this).attr("linha_treino")).val(ui.item.id);
        }
    });   
    
    $("#salvar_treino_aluno").click(function () {
        $("#form_treino_aluno").submit();
    });

    $( "#vincular_aluno" ).autocomplete({
        source: function( request, response ) {
          $.ajax( {
            url: "/servicos_background/autocomplete/alunosSemPersonal.php",
            dataType: "json",
            data: {
              term: request.term
            },
            success: function( data ) {
              response( data );
            }
          } );
        },
        minLength: 2,
        select: function( event, ui ) {
          $("#id_aluno").val(ui.item.id);
        }
    });
    $("#confirmar_vinculacao").click(function () { 
        $("#alert_vinculacao").hide("fade");
        var id_aluno = $("#id_aluno").val();
        var id_personal = $("#id_personal").val();
        $.ajax({
            url: "/vincular_aluno_personal/"+id_aluno+"/"+id_personal,
            type: "GET",            
            dataType: "json",
            success: function (response) {
                if (response.error > 0) {
                    $("#alert_vinculacao").html("<div class='alert alert-danger'>"+response.mensagem+"</div>");
                    $("#alert_vinculacao").show("fade");
                } else {
                    $("#alert_vinculacao").html("<div class='alert alert-success'>"+response.mensagem+"</div>");
                    $("#alert_vinculacao").show("fade");
                }
                
            }
        });
    });
    
    

});

function isEmpty(value){
    if (value == null || value == 0 || value == "0" || value == "" || value ==  undefined || value == "N" || value == "n" ) {
        return true;
    }else{
        return false;
    }
}




