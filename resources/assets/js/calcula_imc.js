/*!
    * Start Bootstrap - SB Admin v6.0.1 (https://startbootstrap.com/templates/sb-admin)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-sb-admin/blob/master/LICENSE)
    */
    (function($) {
    "use strict";

    // // Add active state to sidbar nav links
    // var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
    //     $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function() {
    //         if (this.href === path) {
    //             $(this).addClass("active");
    //         }
    //     });

    // // Toggle the side navigation
    // $("#sidebarToggle").on("click", function(e) {
    //     e.preventDefault();
    //     $("body").toggleClass("sb-sidenav-toggled");
    // });
})(jQuery);

	function calcula_imc() {
        var altura = $("#altura").val().replace(",",".");
        var peso = $("#peso").val().replace(",",".");
        var quadrado = (altura * altura);
        var calculo = (peso / quadrado);
        var message = "Não foi possivel calcular seu IMC ";

        if (calculo < 18.5) {
            message = "Você está abaixo do peso, precisa focar em consumir proteínas e gorduras. Seu índice é: " + calculo;
        }
        else if (calculo >= 18.5 && calculo < 24.9) {
            message = "Seu IMC é ideal. Seu índice é: " + calculo;
        }

        else if (calculo >= 25 && calculo < 29.9) {
            message = "Você está com sobrepeso, e precisa evitar gorduras e carboidratos. Seu índice é: " + calculo;
        }
        else if (calculo >= 30 && calculo < 39.9) {
            message = "Você está com obesidade grau 2, e precisa evitar ao máximo gorduras e carboidratos. Seu índice é: " + calculo;
        }
        else if (calculo > 40){
            message = "Você está com obesidade grau 3, e deve urgentemente consultar um nutricionista. Seu índice é: " + calculo;

        }
        
        $("#resultado_imc_calculado").val(calculo);
		$("#resultado_IMC").show();
		$("#resultado_IMC").html(message);
    }
