// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

const idAluno = $('#id_aluno').val();
const response = $.getJSON(`/servicos_background/api/historico_imc.php?id_aluno=${idAluno}`);

response.then((result) => {
  const labels = [];
  const data = [];

  result.forEach((register) => {
    const { data_registro, valor_imc } = register;

    labels.push(data_registro);
    data.push(Math.round(valor_imc * 100) / 100);
  });

  const ctx = document.getElementById('alunoChartHistoricoImc');
  const myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels,
      datasets: [{
        data,
        label: 'Alunos vinculados',
        lineTension: 0.3,
        backgroundColor: "rgba(255,153,0,0.6)",
        borderColor: "rgba(0,0,0,1)",
        pointRadius: 5,
        pointBackgroundColor: "rgba(0,0,0,1)",
        pointBorderColor: "rgba(0,0,0,0.8)",
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(255,153,0,1)",
        pointHitRadius: 50,
        pointBorderWidth: 2,
      }],
    },
    options: {
      scales: {
        xAxes: [{
          time: {
            unit: 'date'
          },
          gridLines: {
            display: false
          },
          ticks: {
            maxTicksLimit: 7
          }
        }],
        yAxes: [{
          ticks: {
            maxTicksLimit: 10
          },
          gridLines: {
            color: "rgba(0, 0, 0, .125)",
          }
        }],
      },
      legend: {
        display: false
      }
    }
  });
});
