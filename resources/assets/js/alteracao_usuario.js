$(function () {
    $("#data_nasc").change(function () { 
        var data_nasc = $(this).val();
        var data_nasc_format = "";
        data = data_nasc.split("/");
        data_nasc_format = data[2]+"-"+data[1]+"-"+data[0];
        $("#data_nasc_format").val(data_nasc_format);        
    });

    $("#alterar_cadastro").click(function () {
        var nova_senha = $("#nova_senha").val();
        var confirmar_senha = $("#confirmar_senha").val();
        if (!isEmpty(nova_senha)) {
            if (nova_senha != confirmar_senha) {
                alert("Favor confirmar a senha corretamente!");
                return false;
            }
        }
        $("#form_alterar_cadastro_usuario").submit();
        
    });
});

function isEmpty(value){
    if (value == null || value == 0 || value == "0" || value == "" || value ==  undefined || value == "N" || value == "n" ) {
        return true;
    }else{
        return false;
    }
}




