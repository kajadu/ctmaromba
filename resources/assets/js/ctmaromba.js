$(function () {        
    var page = $("#page").val();
    $(".nav-item").removeClass("active");
    $("#"+page).addClass("active");

    $(document).on("keypress",function (key) {         
        if (key.which == 13) {
            $(".btn-submit").click();
        }
    });

    $(".campo-login").focus(function () { 
        $("#result_login").hide("fade");
        $("#result_login").html("");
    });

    $(".form-cadastro-aluno").focus(function () {
        $("#result_cadastro").hide("fade");
    });

     // Add active state to sidbar nav links
     var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
     $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function() {
         if (this.href === path) {
             $(this).addClass("active");
         }
     });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
    });

    $("#cadastrar_aluno").click(function () { 
       
        $.ajax({
            url: "/cadastrar_aluno",
            type: "POST",
            data: $("#cadastro_aluno").serialize(),
            dataType: "json",
            success: function (response) {
                if(response.error > 0){
                    $("#result_cadastro").html("<span>"+response.mensagem+"!</span>");
                    $("#result_cadastro").show("fade");
                }else{
                    $("#result_cadastro").html("<span>"+response.mensagem+"!</span>");
                    $("#result_cadastro").show("fade");
                }
            }
        });
        
    });

    

    
    $(document).on("click","#resetar_treino_aluno",function () { 
        var id_aluno = $(this).attr("id_aluno");
    
        $.ajax({
            url: "/resetar_treino_aluno/"+id_aluno,
            type: "GET",            
            dataType: "json",
            success: function (response) {                
                
                
                window.location.href = "/treino_aluno";
                
            }
        });
    });
    
    $(".efetuar-logout").click(function () {
        window.location.href = "/efetuar_logout";
    });
    
    $("#data_nascimento").mask('99/99/9999');
    $("#cpf_aluno").mask('999.999.999-99');
    // $(document).on("focus","#data_nascimento",function () { 
    //     var data_nascimento = $(this).val();
    //     // data_nascimento = data_nascimento.replace("^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02])) \/\d{4}$");
    //     console.log(data_nascimento);
    //     return false;
        
    // });
    $("#efetuar_login").click(function () { 
        $.ajax({
            url: "/efetuar_login",
            type: "post",
            data: $("#form_efetuar_login").serialize(),
            dataType: "json",
            success: function (response) {                
                if (response.error > 0) {
                   $("#result_login").html(response.mensagem+" <i class='fas fa-exclamation-circle'></i>");
                   $("#result_login").show("fade");
                }else{
                    window.location.href = response.url;
                }
                // result_login
            }
        });
    });

    $(".campo-imc").focus(function () { 
        $("#resultado_IMC").hide("fade");
        $("#resultado_IMC").html("");
    });

    $("#calcula_imc").click(function () { 
        var altura = $("#altura").val().replace(",",".");
        var peso = $("#peso").val().replace(",",".");
        var quadrado = (altura * altura);
        var calculo = (peso / quadrado);
        var message = "Não foi possivel calcular seu IMC ";

        if (calculo < 18.5) {
            message = "Você está abaixo do peso, precisa focar em consumir proteínas e gorduras. Seu índice é: " + calculo;
        }
        else if (calculo >= 18.5 && calculo < 24.9) {
            message = "Seu IMC é ideal. Seu índice é: " + calculo;
        }

        else if (calculo >= 25 && calculo < 29.9) {
            message = "Você está com sobrepeso, e precisa evitar gorduras e carboidratos. Seu índice é: " + calculo;
        }
        else if (calculo >= 30 && calculo < 39.9) {
            message = "Você está com obesidade grau 2, e precisa evitar ao máximo gorduras e carboidratos. Seu índice é: " + calculo;
        }
        else if (calculo > 40){
            message = "Você está com obesidade grau 3, e deve urgentemente consultar um nutricionista. Seu índice é: " + calculo;

        }
        
		$("#resultado_IMC").html(message);
		$("#resultado_IMC").show("fade");
    });  

    $("#registrar_imc").click(function () { 
        $.ajax({
            type: "POST",
            url: "/registrar_imc_aluno",
            data: {
                "id_aluno":$("#id_aluno").val(),
                "valor_imc":$("#resultado_imc_calculado").val()
            },
            dataType: "json",
            success: function (response) {
                location.reload();
            }
        });
       
    });
    
});

function isEmpty(value){
    if (value == null || value == 0 || value == "0" || value == "" || value ==  undefined || value == "N" || value == "n" ) {
        return true;
    }else{
        return false;
    }
}



