$(function () {        
    $(".alterar-usuario").click(function () { 
        var id_usuario = $(this).attr("id_usuario");
        var cargo = $(this).attr("cargo");
        
        $(".tipos-usuarios").show();
        $("#opcao_"+cargo).hide();
        $("#id_usuario_alteracao").val(id_usuario);
        $("#cargo_de").val(cargo);
    });

    $("#alterar_cargo_usuario").attr("disabled", false);
    $("#alterar_cargo_usuario").click(function () {
        $(this).attr("disabled", true);
        var id_usuario =  $("#id_usuario_alteracao").val();
        var cargo_de = $("#cargo_de").val();
        var cargo_para = $('input[name ="tipo_usuario"]:checked').val();

        $.ajax({
            type: "GET",
            url: "/alterar_cargo_usuario/"+id_usuario+"/"+cargo_de+"/"+cargo_para,            
            dataType: "json",
            success: function (response) {
                if (response.error > 0) {
                    alert(response.mensagem);
                } else {
                    location.reload();
                }
            }
        });
    });
});

function isEmpty(value){
    if (value == null || value == 0 || value == "0" || value == "" || value ==  undefined || value == "N" || value == "n" ) {
        return true;
    }else{
        return false;
    }
}




