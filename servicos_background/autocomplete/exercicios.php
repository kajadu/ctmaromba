<?php
$conn = getConnectionPdo();

$term = filter_var($_REQUEST['term'], FILTER_SANITIZE_STRING , FILTER_NULL_ON_FAILURE);
$term = filter_var($term, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
$term = filter_var($term, FILTER_SANITIZE_MAGIC_QUOTES, FILTER_NULL_ON_FAILURE);

if(strlen($term) < 3){
	echo strlen($term);
	die();
}

$term = str_replace('&#38;','&',$term);

$sql = "SELECT 
			tipos_exercicios.id_tipo_exercicio,
			tipos_exercicios.desc_tipo_exercicio,
			musculos.descricao AS descricao_musculo
		FROM tipos_exercicios
		INNER JOIN musculos ON musculos.id_musculo = tipos_exercicios.id_musculo
		WHERE tipos_exercicios.desc_tipo_exercicio LIKE '%{$term}%'";
$stmt = $conn->query($sql);
$result = $stmt->fetchAll();
$dados = [];

if (!empty($result)) {
	foreach ($result as $key => $tipo_exercicio) {
		array_push($dados,array(
			"id" => $tipo_exercicio["id_tipo_exercicio"],
			"label" => $tipo_exercicio["desc_tipo_exercicio"],
			"musculo" => $tipo_exercicio["descricao_musculo"]
		));
	}
}
header("Content-Type: application/json");
echo json_encode($dados);


function getConnectionPdo(){	
	try {
		$username = "u873527492_ctmaromba";
		$password = "=R1hEqwGxP2t";
		$host = "31.170.166.166";
	
		$conn = new \PDO ("mysql:host={$host};port=3306;dbname=u873527492_CTMAROMBA",
					$username,
					$password
				);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		return $conn;
	} catch(\PDOException $e) {
		echo 'ERROR: ' . $e->getMessage();
	}
}