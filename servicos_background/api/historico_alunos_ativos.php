<?php
$conn = getConnectionPdo();

function buildAlunosAtivos($f1, $f2, $f3, $f4) {
    return [
        'alunos' => $f1,
        'mes' => $f2,
        'ano' => $f3,
        'date' => $f4,
    ];
}

$sql = "
SELECT
    numero_alunos_ativos,
    LPAD(mes, 2, '0') as `month`,
    ano as `year`,
    CONCAT(LPAD(mes, 2, '0'), '/', ano)
FROM historico_alunos_ativos
ORDER BY
    `year` ASC,
    `month` ASC
";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->fetchAll(\PDO::FETCH_FUNC, 'buildAlunosAtivos');

header("Content-Type: application/json");
echo json_encode($result);

function getConnectionPdo()
{
    try {
        $username = "u873527492_ctmaromba";
        $password = "=R1hEqwGxP2t";
        $host = "31.170.166.166";

        $conn = new \PDO ("mysql:host={$host};port=3306;dbname=u873527492_CTMAROMBA",
            $username,
            $password
        );
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $conn;
    } catch (\PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
}
