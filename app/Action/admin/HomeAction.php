<?php
namespace App\Action\Admin;

/** 

*
* @author Luis Gabriel
* @version 0.1 
* @access public  
* @example Classe HomeAction
*/ 
use \App\Action\Action as Action;

final class HomeAction extends Action{

  public function index($request, $response, $args){
    
    return $this->view->render($response, 'pages/index.phtml');
    
  }

  public function sobre($request, $response, $args){
    
    return $this->view->render($response, 'pages/sobre.phtml');
    
  }

  public function imc($request, $response, $args){
    
    return $this->view->render($response, 'pages/imc.phtml');
    
  }

  public function login($request, $response, $args){
    
    return $this->view->render($response, 'pages/login.phtml');
    
  }

  public function cadastro($request, $response, $args){
    
    return $this->view->render($response, 'pages/cadastro.phtml');
    
  }

  public function loginAdmin($request, $response, $args){
    session_start();
    if (!isset($_SESSION["usuario"])) {
      return $response->withHeader('Location', '/login');
    } elseif($_SESSION["tipo_usuario"] != "admin") {
      return $response->withHeader('Location', '/login');
    }else{      
      $data["usuario"] = $_SESSION["first_name"];
      $data["id_usuario"] = $_SESSION["id_usuario"];
      return $this->view->render($response, 'pages/login_admin.phtml',$data);
    }
  }

  public function loginPersonal($request, $response, $args){
    session_start();
    if (!isset($_SESSION["usuario"])) {
      return $response->withHeader('Location', '/login');
    } elseif($_SESSION["tipo_usuario"] != "personal") {
      return $response->withHeader('Location', '/login');
    }else{
      $data["usuario"] = $_SESSION["first_name"];
      $data["id_usuario"] = $_SESSION["id_usuario"];      
      return $this->view->render($response, 'pages/login_personal.phtml',$data);
    }
    
  }
  public function loginAluno($request, $response, $args){
    session_start();

    if (!isset($_SESSION["usuario"])) {

      return $response->withHeader('Location', '/login');

    } elseif($_SESSION["tipo_usuario"] != "aluno") {

      return $response->withHeader('Location', '/login');

    }else{
      $data["usuario"] = $_SESSION["first_name"];
      $data["id_usuario"] = $_SESSION["id_usuario"];
      return $this->view->render($response, 'pages/login_aluno.phtml',$data);
    }
  }

  public function alterarUsuarios($request, $response, $args){
    session_start();
    if (!isset($_SESSION["usuario"])) {
      return $response->withHeader('Location', '/login');
    } elseif($_SESSION["tipo_usuario"] != "admin") {
      return $response->withHeader('Location', '/login');
    }else{
      $data["usuario"] = $_SESSION["first_name"];
      $data["id_usuario"] = $_SESSION["id_usuario"];
      $usuarios = new \classes\dao\Usuario ();
      $listaUsuariosAtivos = $usuarios->listarUsuariosAtivos();
      $data["lista_usuarios"] = $listaUsuariosAtivos;
            
      return $this->view->render($response, 'pages/alterar_usuarios.phtml',$data);
    }
    
  }


  public function testeAddUser($request, $response, $args){
    
    $usuario = new \classes\dao\Usuario ();
    
    $senha = md5("teste123");
    $sql = "INSERT INTO personais SET 
                    login_usuario = '46609328854',
                    senha = '{$senha}',
                    nome_completo = 'MURILO HENRIQUE PUCCI',
                    cpf = '46609328854',
                    data_nasc = '1997-03-05',
                    email = 'murilo.pucci@gmail.com'
                    ";
    $stmt = $usuario->conn->prepare($sql);
    $stmt->execute();

    echo "ok";
    
  }

  public function alunosPersonal($request, $response, $args){
    session_start();    
    if (!isset($_SESSION["usuario"])) {
      return $response->withHeader('Location', '/login');
    }else{
      if ($_SESSION["tipo_usuario"] != "personal") {
        return $response->withHeader('Location', '/login');
      }else{
        $personal = new \classes\dao\Personal ();
        $personal->id_usuario = $_SESSION['id_usuario'];
        
        $alunosPersonal = $personal->localizarAlunos();
        
        $listaExercicios = [
          "A"=>[1,2,3,4,5,6,7,8],
          "B"=>[1,2,3,4,5,6,7,8],
          "C"=>[1,2,3,4,5,6,7,8],
          "D"=>[1,2,3,4,5,6,7,8],
          "E"=>[1,2,3,4,5,6,7,8]          
        ];
        $data["usuario"] = $_SESSION["first_name"];
        $data["alunos_personal"] = $alunosPersonal;
        $data["lista_exercicios"] = $listaExercicios;

        
        return $this->view->render($response, 'pages/alunos_personal.phtml',$data);
      }
    }
  }

  public function treinoAluno($request, $response, $args){
    session_start();    
    if (!isset($_SESSION["usuario"])) {
      return $response->withHeader('Location', '/login');
    }else{
      if ($_SESSION["tipo_usuario"] != "aluno") {
        return $response->withHeader('Location', '/login');
      }else{
        $aluno = new \classes\dao\Aluno ();
        $aluno->id_usuario = $_SESSION['id_usuario'];
        $styles = [
          "A" => ["background" => "bg-primary" , "text" => "text-white"],
          "B" => ["background" => "bg-success" , "text" => "text-white"],
          "C" => ["background" => "bg-info" , "text" => "text-white"],
          "D" => ["background" => "bg-warning" , "text" => "text-dark"],
          "E" => ["background" => "bg-dark" , "text" => "text-white"]
        ];
        $personal = $aluno->contemPersonal();

        if (empty($personal)) {
          $data["usuario"] = $_SESSION["first_name"];
          $data["id_usuario"] = $_SESSION["id_usuario"];
          $data["treino_aluno"] = [];
          $data["styles"] = $styles;
          $data["warning"] = "Você não foi atribuido a um personal.";
          $data["warning_style"] = "alert alert-danger";

          return $this->view->render($response, 'pages/treino_aluno.phtml', $data);
        }

        $treino_aluno = $aluno->buscarTreino();

        if(empty($treino_aluno)){

          $data["usuario"] = $_SESSION["first_name"];
          $data["id_usuario"] = $_SESSION["id_usuario"];
          $data["treino_aluno"] = $treino_aluno;
          $data["styles"] = $styles;
          $data["warning"] = "Não há treino atribuido, por gentileza contate o personal <b>{$personal["nome_completo"]}</b>";
          $data["warning_style"] = "alert alert-warning";

          return $this->view->render($response, 'pages/treino_aluno.phtml', $data);

        }

        

        $data["usuario"] = $_SESSION["first_name"];
        $data["id_usuario"] = $_SESSION["id_usuario"];
        $data["treino_aluno"] = $treino_aluno;
        $data["styles"] = $styles;
        $data["warning"] = "Aqui você encontra o treino que foi atribuido a você por um personal. Você pode solicitar a alteração dele após o tempo combinado com o personal.";
        $data["warning_style"] = "";

        return $this->view->render($response, 'pages/treino_aluno.phtml', $data);
        


      }
    }
  }

  public function treinoAlunoConsulta($request, $response, $args){
    session_start();
    if (!isset($_SESSION["usuario"])) {
      return $response->withHeader('Location', '/login');
    }else{
      if ($_SESSION["tipo_usuario"] != "personal") {
        return $response->withHeader('Location', '/login');
      }else{
        $aluno = new \classes\dao\Aluno();
        $aluno->id_usuario = $args["id_aluno"];
        $aluno->buscarAlunoById();
        

        $styles = [
          "A" => ["background" => "bg-primary" , "text" => "text-white"],
          "B" => ["background" => "bg-success" , "text" => "text-white"],
          "C" => ["background" => "bg-info" , "text" => "text-white"],
          "D" => ["background" => "bg-warning" , "text" => "text-dark"],
          "E" => ["background" => "bg-dark" , "text" => "text-white"]
        ];
       
        $treino_aluno = $aluno->buscarTreino();

        $data["usuario"] = $_SESSION["first_name"];
        $data["id_usuario"] = $_SESSION["id_usuario"];
        $data["nome_aluno"] = $aluno->nome_completo;
        $data["treino_aluno"] = $treino_aluno;
        $data["styles"] = $styles;

        return $this->view->render($response, 'pages/treino_aluno_consulta.phtml', $data);
        


      }
    }
  }

  public function alterarCadastroAdmin($request, $response, $args){
    session_start();    
    if (!isset($_SESSION["usuario"])) {
      return $response->withHeader('Location', '/login');
    }else{
      if ($_SESSION["tipo_usuario"] != "admin") {
        return $response->withHeader('Location', '/login');
      }else{
        $usuario = new \classes\dao\Usuario();
        $usuario->id_usuario = $_SESSION["id_usuario"];
        $usuario->tipo_usuario = $_SESSION["tipo_usuario"];
        $usuario->table = "administradores";
        $usuario->campo_id = "id_admin";
        $usuario->buscarUsuarioById();
        
        $data["usuario"] = $_SESSION["first_name"];
        $data["id_usuario"] = $_SESSION["id_usuario"];
        $data["tipo_usuario"] = $usuario->tipo_usuario;
        $data["nome_completo"] = $usuario->nome_completo;
        $data["cpf"] = $usuario->cpf;
        $data["data_nasc"] = $usuario->data_nasc;
        $data["email"] = $usuario->email;

        return $this->view->render($response, 'pages/alterar_cadastro_admin.phtml',$data);
      }
    }
  }

  public function alterarCadastroPersonal($request, $response, $args){
    session_start();
    if (!isset($_SESSION["usuario"])) {
      return $response->withHeader('Location', '/login');
    }else{
      if ($_SESSION["tipo_usuario"] != "personal") {
        return $response->withHeader('Location', '/login');
      }else{
        $usuario = new \classes\dao\Usuario();
        $usuario->id_usuario = $_SESSION["id_usuario"];
        $usuario->tipo_usuario = $_SESSION["tipo_usuario"];
        $usuario->table = "personais";
        $usuario->campo_id = "id_personal";
        $usuario->buscarUsuarioById();

        $data["usuario"] = $_SESSION["first_name"];
        $data["id_usuario"] = $_SESSION["id_usuario"];
        $data["tipo_usuario"] = $usuario->tipo_usuario;
        $data["nome_completo"] = $usuario->nome_completo;
        $data["cpf"] = $usuario->cpf;
        $data["data_nasc"] = $usuario->data_nasc;
        $data["email"] = $usuario->email;

        return $this->view->render($response, 'pages/alterar_cadastro_personal.phtml',$data);
      }
    }
  }

  public function alterarCadastroAluno($request, $response, $args){
    session_start();
    if (!isset($_SESSION["usuario"])) {
      return $response->withHeader('Location', '/login');
    }else{
      if ($_SESSION["tipo_usuario"] != "aluno") {
        return $response->withHeader('Location', '/login');
      }else{
        $result = [];
        $usuario = new \classes\dao\Usuario();
        $usuario->id_usuario = $_SESSION["id_usuario"];
        $usuario->tipo_usuario = $_SESSION["tipo_usuario"];
        $usuario->table = "alunos";
        $usuario->campo_id = "id_aluno";
        $usuario->buscarUsuarioById();

        $data["usuario"] = $_SESSION["first_name"];
        $data["id_usuario"] = $_SESSION["id_usuario"];
        $data["tipo_usuario"] = $usuario->tipo_usuario;
        $data["nome_completo"] = $usuario->nome_completo;
        $data["cpf"] = $usuario->cpf;
        $data["data_nasc"] = $usuario->data_nasc;
        $data["email"] = $usuario->email; 

        return $this->view->render($response, 'pages/alterar_cadastro_aluno.phtml',$data);
      }
    }
  }

  

}
