<?php

/** 
*
*
* @author Luis Gabriel
* @version 0.1 
* @access public  
* @example Classe ApiAction
*/ 
namespace App\Action\Api;

use \App\Action\Action as Action;
final class ApiAction extends Action{

  public function index($request, $response){
  }  

  public function efetuarLogin($request, $response){
    $args = $request->getParsedBody();
    
    $result = [];

    if (empty($args["login"])) {
      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("Favor digitar o login");
      return $response->withJson($result);
    }elseif ( empty($args["senha"])) {
      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("Favor digitar a senha");
      return $response->withJson($result);
    }else{
      $usuario = new \classes\dao\Usuario ();
      $usuario->login_usuario = $args["login"];
      $usuario->senha = md5($args["senha"]);
      $usuario->efetuarLogin();

      if (!empty($usuario->id_usuario)) {
        $result["error"] = 1;
        $result["mensagem"] = utf8_encode("Login ou senha inv�lidos"); 
        return $response->withJson($result);
      }
      $tipo_usuario = $usuario->tipo_usuario;
      switch ($tipo_usuario) {
        case 'admin':
          $result["error"] = 0;
          $result["mensagem"] = "";
          $result["url"] = "/admin";          
          break;
        case 'personal':
          $result["error"] = 0;
          $result["mensagem"] = "";
          $result["url"] = "/personal";
          break;
        case 'aluno':
          $result["error"] = 0;
          $result["mensagem"] = "";
          $result["url"] = "/aluno";
          break;
        default:
          $result["error"] = 1;
          $result["mensagem"] = utf8_encode("Login ou senha inv�lidos");           
          break;
      }
      return $response->withJson($result);
    }
  }
  public function efetuarLogout($request, $response){
    session_start();    
    unset($_SESSION["usuario"]);
    return $response->withHeader('Location', '/login');
  }

  public function buscaExercicios($request, $response){
    $args = $request->getParsedBody();
    $term = $args["term"];
    $exercicio = new \classes\dao\Exercicio();
    $dados = $exercicio->localizarExercicioAutoComplete($term);
    return $response->withJson($dados);
  }

  public function salvarTreinoAluno($request, $response){
    $args = $request->getParsedBody();
    $tipos_treinos = ["A", "B", "C", "D", "E"];
    
    foreach ($tipos_treinos as $tipo_treino) {
      foreach ($args["id_tipo_exercicio_{$tipo_treino}"] as $key => $id_tipo_exercicio) {
        if(!empty($id_tipo_exercicio)){
          $exercicio = new \classes\dao\Treino();
          $exercicio->tipo_treino = $tipo_treino;
          $exercicio->id_aluno = $args["id_aluno_modal"];
          $exercicio->id_exercicio = $id_tipo_exercicio;
          $exercicio->series = $args["series_{$tipo_treino}"][$key];
          $exercicio->repeticoes = $args["repeticoes_{$tipo_treino}"][$key];
          
          $exercicio->cadastrarTreino();
        }
      }
    }
    
    return $response->withHeader('Location', '/alunos_personal');
  
  }

  public function cadastrarAluno($request, $response){
    $args = $request->getParsedBody();
    
    $aluno = new \classes\dao\Aluno();
    $result=[];

    if (empty($args["nome_aluno"])) {

      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("Favor digitar seu nome para prosseguir com seu cadastro");
      return $response->withJson($result);

    }elseif (empty($args["cpf_aluno"])) {

      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("Favor digitar seu CPF para prosseguir com seu cadastro");
      return $response->withJson($result);

    }elseif (empty($args["data_nascimento"])) {

      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("Favor digitar sua data de nascimento para prosseguir com seu cadastro");
      return $response->withJson($result);

    }elseif (empty($args["email_aluno"])) {

      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("Favor digitar seu email para prosseguir com seu cadastro");
      return $response->withJson($result);

    }elseif (empty($args["senha_aluno"])) {

      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("Favor digitar sua senha para prosseguir com seu cadastro");
      return $response->withJson($result);

    }else{
      $cpf_aluno = str_replace([".","-"],"",$args["cpf_aluno"]);

      $aluno->login_usuario = $cpf_aluno;
      $aluno->senha = md5($args["senha_aluno"]);
      $aluno->nome_completo = strtoupper($args["nome_aluno"]);
      $aluno->cpf = $cpf_aluno;      

      $data_nascimento = new \DateTime(str_replace("/","-",$args["data_nascimento"]));
      
      $aluno->data_nasc = $data_nascimento->format("Y-m-d");

      $aluno->email = $args["email_aluno"];

      $aluno->consultarCadastro();
      $id_aluno = $aluno->id_usuario;
      if(!empty($id_aluno)){
        $result["error"] = 1;
        $result["mensagem"] = utf8_encode("Usu�rio j� cadastrado");
        return $response->withJson($result);
      }else{
        $id_aluno_cadastrado = $aluno->cadastroAluno();
        if(empty($id_aluno_cadastrado)){

          $result["error"] = 1;
          $result["mensagem"] = utf8_encode("Houve um problema no cadastro do aluno, j� estamos verificando");
          return $response->withJson($result);

        }else{

          $result["error"] = 1;
          $result["mensagem"] = utf8_encode("Usu�rio cadastrado de acordo");
          return $response->withJson($result);

        }
      }

    }
    
  }

  public function resetarTreinoAluno($request, $response, $args){
    $result = [];
    $id_aluno = $args["id_aluno"];
    if(empty($id_aluno)){

      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("N�o foi localizado o ID do aluno");
      return $response->withJson($result);

    }else{
      $treino = new \classes\dao\Treino();
      $treino->id_aluno = $id_aluno;
      $treino->resetarTreinoAluno();

      $result["error"] = 0;
      $result["mensagem"] = utf8_encode("Treino resetado de acordo, favor solicitar um novo treino para o seu personal");
      return $response->withJson($result);
      
    }
    //return $response->withHeader('Location', '/login');
  }

  public function alterarCargoUsuario($request, $response, $args){
    $result = [];
    $id_usuario = $args["id_usuario"];

    if (empty($id_usuario)) {
      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("N�o foi localizado o ID do usuario");
      return $response->withJson($result);
    }

    if (empty($args["cargo_de"])) {
      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("N�o foi localizado o cargo do usuario");
      return $response->withJson($result);
    }

    if (empty($args["cargo_para"])) {
      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("N�o foi localizado o novo cargo do usuario");
      return $response->withJson($result);
    }

    $usuario = new \classes\dao\Usuario();
    $usuario->id_usuario = $id_usuario;

    switch ($args["cargo_de"]) {
      case 'ADMIN':        
        $usuario->table = "administradores";
        $usuario->campo_id = "id_admin";
        break;
      case 'PERSONAL':        
        $usuario->table = "personais";
        $usuario->campo_id = "id_personal";
        break;
      case 'ALUNO':
        $usuario->table = "alunos";
        $usuario->campo_id = "id_aluno";
        break;
    }

    switch ($args["cargo_para"]) {
      case 'ADMIN':        
        $usuario->table_para = "administradores";        
        break;
      case 'PERSONAL':        
        $usuario->table_para = "personais";        
        break;
      case 'ALUNO':
        $usuario->table_para = "alunos";
        break;
    }

    $usuario->alterarCargo();
    $result["error"] = 0;
    $result["mensagem"] = "";
    return $response->withJson($result);

  }
  public function alterarCadastroUsuario($request, $response){
    $args = $request->getParsedBody();
    $usuario = new \classes\dao\Usuario();

    switch ($args["tipo_usuario"]) {
      case 'admin':        
        $usuario->table = "administradores";
        $usuario->campo_id = "id_admin";
        break;
      case 'personal':        
        $usuario->table = "personais";
        $usuario->campo_id = "id_personal";
        break;
      case 'aluno':
        $usuario->table = "alunos";
        $usuario->campo_id = "id_aluno";
        break;
    }
    $usuario->id_usuario = $args["id_usuario"];
    $usuario->tipo_usuario = $args["tipo_usuario"];
    $usuario->nome_completo = $args["nome_completo"];
    $usuario->cpf = $args["cpf"];
    $usuario->data_nasc = $args["data_nasc_format"];
    $usuario->email = $args["email"];

    $usuario->atualizarCadastroUsuario();

    if (!empty($args["nova_senha"])) {
      $usuario->senha = md5($args["nova_senha"]);
      $usuario->atualizarSenhaUsuario();
    }

    return $response->withHeader('Location', "/alterar_cadastro_{$usuario->tipo_usuario}");   
    
  }  

  public function vincularAlunoPersonal($request, $response, $args){
    $result=[];
    
    $aluno = new \classes\dao\Aluno();
    $aluno->id_usuario = $args["id_aluno"];
    $alunos = $aluno->contemPersonal();

    if(!empty($alunos)){
      $result["error"] = 1;
      $result["mensagem"] = utf8_encode("Aluno j� foi vinculado a um personal");
      return $response->withJson($result);
    }

    $personal = new \classes\dao\Personal();
    $personal->id_usuario = $args["id_personal"];
    $personal->id_aluno = $args["id_aluno"];
    $personal->vincularAlunoById();
    $result["error"] = 0;
    $result["mensagem"] = utf8_encode("Aluno vinculado de acordo");
    return $response->withJson($result);

  }
  public function registrarImcAluno($request, $response){
    $args = $request->getParsedBody();
    $aluno = new \classes\dao\Aluno();
    $aluno->id_usuario = $args["id_aluno"];
    $aluno->valor_imc = $args["valor_imc"];
    $aluno->registrarImcAluno();
    $result["error"] = 0;
    $result["mensagem"] = utf8_encode("IMC registrado de acordo");
    return $response->withJson($result);
    
  }
  

  
}
