<?php

//Telas Gest�o Assinaturas
$app->get('/','App\Action\admin\HomeAction:index');
$app->get('/sobre','App\Action\admin\HomeAction:sobre');
$app->get('/imc','App\Action\admin\HomeAction:imc');
$app->get('/login','App\Action\admin\HomeAction:login');
$app->get('/cadastro','App\Action\admin\HomeAction:cadastro');

$app->get('/admin','App\Action\admin\HomeAction:loginAdmin');
$app->get('/personal','App\Action\admin\HomeAction:loginPersonal');
$app->get('/alunos_personal','App\Action\admin\HomeAction:alunosPersonal');
$app->get('/aluno','App\Action\admin\HomeAction:loginAluno');
$app->get('/treino_aluno','App\Action\admin\HomeAction:treinoAluno');
$app->get('/treino_aluno_consulta/{id_aluno}','App\Action\admin\HomeAction:treinoAlunoConsulta');
$app->get('/alterar_usuarios','App\Action\admin\HomeAction:alterarUsuarios');

$app->get('/alterar_cadastro_admin','App\Action\admin\HomeAction:alterarCadastroAdmin');
$app->get('/alterar_cadastro_personal','App\Action\admin\HomeAction:alterarCadastroPersonal');
$app->get('/alterar_cadastro_aluno','App\Action\admin\HomeAction:alterarCadastroAluno');

$app->get('/teste_add_user','App\Action\admin\HomeAction:testeAddUser');


//API
$app->get('/efetuar_logout','App\Action\api\ApiAction:efetuarLogout');

$app->get('/resetar_treino_aluno/{id_aluno}','App\Action\api\ApiAction:resetarTreinoAluno');

$app->get('/alterar_cargo_usuario/{id_usuario}/{cargo_de}/{cargo_para}','App\Action\api\ApiAction:alterarCargoUsuario');

$app->get('/vincular_aluno_personal/{id_aluno}/{id_personal}','App\Action\api\ApiAction:vincularAlunoPersonal');

$app->post('/cadastro_usuario','App\Action\api\ApiAction:cadastroUsuario');

$app->post('/cadastrar_aluno','App\Action\api\ApiAction:cadastrarAluno');

$app->post('/efetuar_login','App\Action\api\ApiAction:efetuarLogin');

$app->post('/busca_exercicios','App\Action\api\ApiAction:buscaExercicios');

$app->post('/salvar_treino_aluno','App\Action\api\ApiAction:salvarTreinoAluno');

$app->post('/alterar_cadastro_usuario','App\Action\api\ApiAction:alterarCadastroUsuario');

$app->post('/registrar_imc_aluno','App\Action\api\ApiAction:registrarImcAluno');






