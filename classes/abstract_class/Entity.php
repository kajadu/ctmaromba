<?php
/**
 * Classe Abstrata de todas as entidades do sistema
 *
 * PHP version 7.1
 *
 * @category  abstract class
 * @package   abstract_class
 * @author    Luis Gabriel 
 */

namespace classes\abstract_class;

abstract class Entity {
    
     protected $conn;
    
     public function __construct() {          
          $this->conn = $this->getConnectionPdo();
	}
 
     /**
     * Metodo magico Retorna o conteudo do valor setado
     *
     * @param Mixed $valor da propriedade da classe
     *
     * @return Mixed retorna o conteudo referente a propriedade solicitada
     */
     public function __get($valor)
     {         
          
          return $this->$valor;
     }

     /**
     * Metodo magico Adiciona o conteudo a propriedade da classe
     *
     * @param Mixed $propriedade da propriedade da classe
     * @param Mixed $valor       da propriedade da classe
     *
     * @return Mixed retorna o conteudo referente a propriedade solicitada
     */
     public function __set($propriedade,$valor)
     {
          $this->$propriedade = $valor;
          
     }

     public function getConnectionPdo()
     {
          
          try {
               $username = "u873527492_ctmaromba";
               $password = "=R1hEqwGxP2t";
               $host = "31.170.166.166";
           
               $conn = new \PDO ("mysql:host={$host};port=3306;dbname=u873527492_CTMAROMBA",
                           $username,
                           $password
                       );
               $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

               return $conn;
          } catch(\PDOException $e) {
               echo 'ERROR: ' . $e->getMessage();
          }
     }
   
    
}
