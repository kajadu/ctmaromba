<?php
/** 
* Classe Entidade do Exercicio onde consta os atributos do mesmo.
*
* @author Luis Gabriel
* @version 0.1  
* @access public  
* @example Classe Exercicio_Entity
*/ 

namespace classes\entity;

class Exercicio_Entity extends \classes\abstract_class\Entity
{
  
  public function __construct() {
    parent::__construct();
  }  
  
  protected $id_tipo_treino;
  protected $id_musculo;
  protected $desc_tipo_treino;
  
}
