<?php
/** 
* Classe Entidade do Personal onde consta os atributos do mesmo.
*
* @author Luis Gabriel
* @version 0.1  
* @access public  
* @example Classe Personal_Entity
*/ 

namespace classes\entity;

class Personal_Entity extends \classes\abstract_class\Entity
{
  
  public function __construct() {
    parent::__construct();
  }  
  
  	protected $id_usuario;
	protected $login_usuario;
	protected $senha;
	protected $nome_completo;
	protected $cpf;
	protected $data_nasc;
	protected $email;
	protected $logradouro;
	protected $numero;
	protected $complemento;
	protected $bairro;
	protected $cidade;
	protected $estado;
	protected $cep;
	protected $ativo;
	protected $data_cadastro;
	protected $data_cancelamento;
	protected $data_reativacao;
	protected $tipo_usuario;	
	protected $id_aluno;


}
