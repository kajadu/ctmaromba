<?php
/** 
* Classe Entidade do Treino onde consta os atributos do mesmo.
*
* @author Luis Gabriel
* @version 0.1  
* @access public  
* @example Classe Treino_Entity
*/ 

namespace classes\entity;

class Treino_Entity extends \classes\abstract_class\Entity
{
  
  public function __construct() {
    parent::__construct();
  }  
  
  	protected $id_treino;
	protected $tipo_treino;
	protected $id_aluno;
	protected $id_exercicio;
	protected $series;
	protected $repeticoes;
  
}
