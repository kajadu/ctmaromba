<?php

/** 
* 
* @author Luis Gabriel
* @version 0.1 
* @access public  
* @example Classe Usuario
*/ 
namespace classes\dao;

class Usuario extends \classes\entity\Usuario_Entity {
     /**
     * Importa o construtor da Classe Comprador_Entity
     */
    public function __construct()   {

        parent::__construct();
       
    }

    public function efetuarLogin(){
        $sql = "SELECT *, id_admin AS id_usuario FROM administradores WHERE login_usuario = '{$this->login_usuario}' AND senha = '{$this->senha}' AND ativo = 1 ";
        $stmt = $this->conn->query($sql);
        $result = $stmt->fetch();

        if (!empty($result)) {
            $this->atribuirValoresBaseClasse($result);
            $this->tipo_usuario = "admin";
        }else{
            $sql = "SELECT *, id_personal AS id_usuario FROM personais WHERE login_usuario = '{$this->login_usuario}' AND senha = '{$this->senha}' AND ativo = 1 ";
            $stmt = $this->conn->query($sql);
            $result = $stmt->fetch();

            if (!empty($result)) {
                $this->atribuirValoresBaseClasse($result);
                $this->tipo_usuario = "personal";
                
            } else {                
                $sql = "SELECT *, id_aluno AS id_usuario FROM alunos WHERE login_usuario = '{$this->login_usuario}' AND senha = '{$this->senha}' AND ativo = 1 ";
                $stmt = $this->conn->query($sql);
                $result = $stmt->fetch();

                if (!empty($result)) {
                    $this->atribuirValoresBaseClasse($result);
                    $this->tipo_usuario = "aluno";                    
                }
            }
        }
        session_start();
        $_SESSION['id_usuario'] = $this->id_usuario;
        $_SESSION['usuario'] = $this->nome_completo;
        $name = explode(" ",$this->nome_completo);
        $first_name = $name[0];
        $_SESSION['first_name'] = $first_name;
        $_SESSION['email'] = $this->email;
        $_SESSION['login_usuario'] = $this->login_usuario;
        $_SESSION['tipo_usuario'] = $this->tipo_usuario;
        
    }

    public function buscarUsuarioById(){
        $sql = "SELECT *, {$this->campo_id} AS id_usuario FROM {$this->table} WHERE {$this->campo_id} = '{$this->id_usuario}' AND ativo = 1 ";
        $stmt = $this->conn->query($sql);
        $result = $stmt->fetch();

        
        if (!empty($result)) {
            $this->atribuirValoresBaseClasse($result);                
        }
    }

    public function desativarUsuarioById(){
        $date = new \Datetime("now");
        $data_atual = $date->format("Y-m-d");
        $sql = "UPDATE {$this->table} SET 
                    ativo = 0,
                    data_cancelamento = '{$data_atual}'
                    WHERE {$this->campo_id} = '{$this->id_usuario}'";        
        
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function atualizarCadastroUsuario(){
        $sql = "UPDATE {$this->table} SET                     
                    nome_completo = '{$this->nome_completo}',
                    cpf = '{$this->cpf}',
                    data_nasc = '{$this->data_nasc}',
                    email = '{$this->email}'
                    WHERE {$this->campo_id} = '{$this->id_usuario}'";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function atualizarSenhaUsuario(){
        $sql = "UPDATE {$this->table} SET
                    senha = '{$this->senha}'
                    WHERE {$this->campo_id} = '{$this->id_usuario}'";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function incluirNovoCargoUsuario(){
        
        $sql = "INSERT INTO {$this->table_para}
                    SET                         
                        login_usuario = '{$this->login_usuario}',
                        senha = '{$this->senha}',
                        nome_completo = '{$this->nome_completo}',
                        cpf = '{$this->cpf}',
                        data_nasc = '{$this->data_nasc}',
                        email = '{$this->email}',
                        logradouro = '{$this->logradouro}',
                        numero = '{$this->numero}',
                        complemento = '{$this->complemento}',
                        bairro = '{$this->bairro}',
                        cidade = '{$this->cidade}',
                        estado = '{$this->estado}',
                        cep = '{$this->cep}'
                    ";        
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    

    public function atribuirValoresBaseClasse($result){
        $this->id_usuario = $result["id_usuario"];
        $this->login_usuario = $result["login_usuario"];
        $this->senha = $result["senha"];
        $this->nome_completo = $result["nome_completo"];
        $this->cpf = $result["cpf"];
        $this->data_nasc = $result["data_nasc"];
        $this->email = $result["email"];
        $this->logradouro = $result["logradouro"];
        $this->numero = $result["numero"];
        $this->complemento = $result["complemento"];
        $this->bairro = $result["bairro"];
        $this->cidade = $result["cidade"];
        $this->estado = $result["estado"];
        $this->cep = $result["cep"];
        $this->ativo = $result["ativo"];
        $this->data_cadastro = $result["data_cadastro"];
        $this->data_cancelamento = $result["data_cancelamento"];
        $this->data_reativacao = $result["data_reativacao"];
        
    }

    public function listarUsuariosAtivos(){
        
        $sql_personais = "SELECT *, id_personal AS id_usuario FROM personais WHERE ativo = 1 ";
        $stmt_personais = $this->conn->query($sql_personais);
        $personais = $stmt_personais->fetchAll();

        foreach ($personais as $key => $personal) {
            $personais[$key]["cargo"] = "PERSONAL";
        }

        
        $sql_alunos = "SELECT *, id_aluno AS id_usuario FROM alunos WHERE ativo = 1 ";
        $stmt_alunos = $this->conn->query($sql_alunos);
        $alunos = $stmt_alunos->fetchAll();

        foreach ($alunos as $key => $aluno) {
            $alunos[$key]["cargo"] = "ALUNO";
        }

        return array_merge($personais,$alunos);
    }

    public function alterarCargo(){
        $this->buscarUsuarioById();
        $this->desativarUsuarioById();
        $this->incluirNovoCargoUsuario();
    }
    
}