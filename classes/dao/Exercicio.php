<?php

/** 
* 
* @author Luis Gabriel
* @version 0.1 
* @access public  
* @example Classe Exercicio
*/ 
namespace classes\dao;

class Exercicio extends \classes\entity\Exercicio_Entity {
     /**
     * Importa o construtor da Classe Comprador_Entity
     */
    public function __construct()   {

        parent::__construct();
       
    }

    public function localizarExercicioAutoComplete($term){
        
        if(strlen($term) < 3){
            echo strlen($term);
            die();
        }

        $term = str_replace('&#38;','&',$term);

        $sql = "SELECT 
                    tipos_exercicios.id_tipo_exercicio,
                    tipos_exercicios.desc_tipo_exercicio,
                    musculos.descricao AS descricao_musculo
                FROM tipos_exercicios
                INNER JOIN musculos ON musculos.id_musculo = tipos_exercicios.id_musculo
                WHERE tipos_exercicios.desc_tipo_exercicio LIKE '%{$term}%'";
        $stmt = $this->conn->query($sql);
        $result = $stmt->fetchAll();
        $dados = [];

        if (!empty($result)) {
            foreach ($result as $key => $tipo_exercicio) {
                $dados[] = array(
                    "id" => $tipo_exercicio["id_tipo_exercicio"],
                    "label" => utf8_encode($tipo_exercicio["desc_tipo_exercicio"]),
                    "musculo" => $tipo_exercicio["descricao_musculo"]
                );
            }
        }

        return $dados;

        
    }

       
}