<?php

/** 
* 
* @author Luis Gabriel
* @version 0.1 
* @access public  
* @example Classe Personal
*/ 
namespace classes\dao;

class Personal extends \classes\entity\Personal_Entity {
     /**
     * Importa o construtor da Classe Comprador_Entity
     */
    public function __construct()   {

        parent::__construct();
       
    }

    public function localizarPersonal(){
        $sql = "SELECT *, id_personal AS id_usuario FROM personais WHERE id_personal = '{$this->id_usuario}' AND ativo = 1 ";
        $stmt = $this->conn->query($sql);
        $result = $stmt->fetch();

        if (!empty($result)) {
            $this->atribuirValoresBaseClasse($result);
            $this->tipo_usuario = "personal";
        }
    }

    public function localizarAlunos(){
        $sql = "SELECT *
                    FROM alunos
                 WHERE alunos.id_personal = '{$this->id_usuario}' AND ativo = 1 ";
        $stmt = $this->conn->query($sql);
        $result = $stmt->fetchAll();

        foreach ($result as $key => $aluno) {
            $sql = "SELECT *
                        FROM treinos
                    WHERE treinos.id_aluno = '{$aluno["id_aluno"]}'";
            $stmt = $this->conn->query($sql);
            $treino = $stmt->fetch();

            if(empty($treino)){
                $result[$key]["contem_treino"] = 0;
            }else{
                $result[$key]["contem_treino"] = 1;
            }
        }
        return $result;
    }

    public function vincularAlunoById(){
        $sql = "UPDATE alunos SET
                    id_personal = {$this->id_usuario}
                WHERE id_aluno = {$this->id_aluno}
                "; 
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function atribuirValoresBaseClasse($result){
        $this->id_usuario = $result["id_usuario"];
        $this->login_usuario = $result["login_usuario"];
        $this->senha = $result["senha"];
        $this->nome_completo = $result["nome_completo"];
        $this->cpf = $result["cpf"];
        $this->data_nasc = $result["data_nasc"];
        $this->email = $result["email"];
        $this->logradouro = $result["logradouro"];
        $this->numero = $result["numero"];
        $this->complemento = $result["complemento"];
        $this->bairro = $result["bairro"];
        $this->cidade = $result["cidade"];
        $this->estado = $result["estado"];
        $this->cep = $result["cep"];
        $this->ativo = $result["ativo"];
        $this->data_cadastro = $result["data_cadastro"];
        $this->data_cancelamento = $result["data_cancelamento"];
        $this->data_reativacao = $result["data_reativacao"];
    }


    
}