<?php

/** 
* 
* @author Luis Gabriel
* @version 0.1 
* @access public  
* @example Classe Treino
*/ 
namespace classes\dao;

class Treino extends \classes\entity\Treino_Entity {
     /**
     * Importa o construtor da Classe Comprador_Entity
     */
    public function __construct()   {

        parent::__construct();
       
    }

    public function cadastrarTreino()
    {   
        $sql = "INSERT INTO treinos SET
                            tipo_treino = '{$this->tipo_treino}',
                            id_aluno = {$this->id_aluno},
                            id_exercicio = {$this->id_exercicio},
                            series = {$this->series},
                            repeticoes = {$this->repeticoes}
                        ";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function resetarTreinoAluno()
    {   
        $sql = "DELETE FROM treinos WHERE id_aluno = {$this->id_aluno}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }
    
}