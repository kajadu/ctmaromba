<?php

/** 
* 
* @author Luis Gabriel
* @version 0.1 
* @access public  
* @example Classe Aluno
*/ 
namespace classes\dao;

class Aluno extends \classes\entity\Aluno_Entity {
     /**
     * Importa o construtor da Classe Comprador_Entity
     */
    public function __construct()   {

        parent::__construct();
       
    }

    // public function efetuarLogin(){
    //     $sql = "SELECT *, id_admin AS id_usuario FROM administradores WHERE login_usuario = '{$this->login_usuario}' AND senha = '{$this->senha}' AND ativo = 1 ";
    //     $stmt = $this->conn->query($sql);
    //     $result = $stmt->fetch();

    //     if (!empty($result)) {
    //         $this->atribuirValoresBaseClasse($result);
    //         $this->tipo_usuario = "admin";
    //     }else{
    //         $sql = "SELECT *, id_personal AS id_usuario FROM personais WHERE login_usuario = '{$this->login_usuario}' AND senha = '{$this->senha}' AND ativo = 1 ";
    //         $stmt = $this->conn->query($sql);
    //         $result = $stmt->fetch();

    //         if (!empty($result)) {
    //             $this->atribuirValoresBaseClasse($result);
    //             $this->tipo_usuario = "personal";
                
    //         } else {                
    //             $sql = "SELECT *, id_aluno AS id_usuario FROM alunos WHERE login_usuario = '{$this->login_usuario}' AND senha = '{$this->senha}' AND ativo = 1 ";
    //             $stmt = $this->conn->query($sql);
    //             $result = $stmt->fetch();

    //             if (!empty($result)) {
    //                 $this->atribuirValoresBaseClasse($result);
    //                 $this->tipo_usuario = "aluno";                    
    //             }
    //         }
    //     }
    //     session_start();
    //     $_SESSION['id_usuario'] = $this->id_usuario;
    //     $_SESSION['usuario'] = $this->nome_completo;
    //     $_SESSION['email'] = $this->email;
    //     $_SESSION['login_usuario'] = $this->login_usuario;
        
    // }
    public function buscarAlunoById(){
        $sql = "SELECT *, id_aluno AS id_usuario FROM alunos WHERE id_aluno = '{$this->id_usuario}' AND ativo = 1 ";
        $stmt = $this->conn->query($sql);
        $result = $stmt->fetch();
        
        if (!empty($result)) {
            $this->atribuirValoresBaseClasse($result);                
        }
    }

    public function buscarTreino(){
        $sql = "SELECT 
                    treinos.id_treino,
                    treinos.tipo_treino,
                    treinos.series,
                    treinos.repeticoes,
                    tipos_exercicios.desc_tipo_exercicio AS desc_exercicio,
                    musculos.descricao AS desc_musculo
                FROM treinos
                INNER JOIN tipos_exercicios ON tipos_exercicios.id_tipo_exercicio = treinos.id_exercicio
                INNER JOIN musculos ON musculos.id_musculo = tipos_exercicios.id_musculo
                WHERE treinos.id_aluno = '{$this->id_usuario}'";
        $stmt = $this->conn->query($sql);
        $result = $stmt->fetchAll();

        

        $treino_aluno = [];
        foreach ($result as $treino) {            

            $treino_aluno[$treino["tipo_treino"]][$treino["id_treino"]]["desc_exercicio"] = $treino["desc_exercicio"];
            $treino_aluno[$treino["tipo_treino"]][$treino["id_treino"]]["desc_musculo"] = $treino["desc_musculo"];
            $treino_aluno[$treino["tipo_treino"]][$treino["id_treino"]]["series"] = $treino["series"];
            $treino_aluno[$treino["tipo_treino"]][$treino["id_treino"]]["repeticoes"] = $treino["repeticoes"];            
        }
        
        return $treino_aluno;
        
    }

    public function consultarCadastro()
    {
        $sql = "SELECT *, id_admin AS id_usuario FROM administradores WHERE cpf = '{$this->cpf}' AND ativo = 1 ";
        $stmt = $this->conn->query($sql);
        $result = $stmt->fetch();

        if (!empty($result)) {
            $this->atribuirValoresBaseClasse($result);            
        }else{
            $sql = "SELECT *, id_personal AS id_usuario FROM personais WHERE cpf = '{$this->cpf}' AND ativo = 1 ";
            $stmt = $this->conn->query($sql);
            $result = $stmt->fetch();

            if (!empty($result)) {
                $this->atribuirValoresBaseClasse($result);
            } else {                
                $sql = "SELECT *, id_aluno AS id_usuario FROM alunos WHERE cpf = '{$this->cpf}' AND ativo = 1 ";
                $stmt = $this->conn->query($sql);
                $result = $stmt->fetch();
                if (!empty($result)) {
                    $this->atribuirValoresBaseClasse($result);                                
                }
            }
        }
        return $this;
    }

    public function cadastroAluno()
    {   
        $sql = "INSERT INTO alunos SET
                                login_usuario = '{$this->login_usuario}',
                                senha = '{$this->senha}',
                                nome_completo = '{$this->nome_completo}',
                                cpf = '{$this->cpf}',
                                data_nasc = '{$this->data_nasc}',
                                email = '{$this->email}'
                                
                            ";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $this->conn->lastInsertId();
    }    

    public function contemPersonal(){
        $sql = "SELECT 
                    personais.id_personal,
                    personais.nome_completo
                FROM alunos
                INNER JOIN personais ON personais.id_personal = alunos.id_personal
                WHERE alunos.id_aluno = '{$this->id_usuario}'";
        $stmt = $this->conn->query($sql);
        $result = $stmt->fetch();

        return $result;
    }

    public function registrarImcAluno(){
        $sql = "INSERT INTO historico_imc_alunos SET
                                id_aluno = '{$this->id_usuario}',
                                valor_imc = '{$this->valor_imc}'
                            ";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }
    

    public function atribuirValoresBaseClasse($result){
        $this->id_usuario = $result["id_usuario"];
        $this->login_usuario = $result["login_usuario"];
        $this->senha = $result["senha"];
        $this->nome_completo = $result["nome_completo"];
        $this->cpf = $result["cpf"];
        $this->data_nasc = $result["data_nasc"];
        $this->email = $result["email"];
        $this->logradouro = $result["logradouro"];
        $this->numero = $result["numero"];
        $this->complemento = $result["complemento"];
        $this->bairro = $result["bairro"];
        $this->cidade = $result["cidade"];
        $this->estado = $result["estado"];
        $this->cep = $result["cep"];
        $this->ativo = $result["ativo"];
        $this->data_cadastro = $result["data_cadastro"];
        $this->data_cancelamento = $result["data_cancelamento"];
        $this->data_reativacao = $result["data_reativacao"];
        
    }


    
}